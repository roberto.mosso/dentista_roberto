@props(['active'])

<nav class="navbar navbar-expand-lg bg-filter-navbar">
  <a class="navbar-brand nav-title" href="{{route("homepage")}}"><i class="fas fa-tooth mr-3"></i>Il dentista</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon d-flex align-items-center justify-content-center"><i class="fas fa-bars"></i></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-links nav-link {{Route::currentRouteName()=='homepage' ? 'active' : ' '}}" href="{{route("homepage")}}">Homepage <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-links nav-link {{Route::currentRouteName()=='chi_siamo' ? 'active' : ' '}}" href="{{route("chi_siamo")}}">Chi siamo</a>
      </li>
      <li class="nav-item">
        <a class="nav-links nav-link {{Route::currentRouteName()=='services.index' ? 'active' : ' '}}" href="{{route("services.index")}}">Cosa facciamo</a>
      </li>
      <li class="nav-item">
        <a class="nav-links nav-link {{Route::currentRouteName()=='contatti' ? 'active' : ' '}}" href="{{route("contatti")}}">Contatti</a>
      </li>
    </ul>
    
  </div>
</nav>