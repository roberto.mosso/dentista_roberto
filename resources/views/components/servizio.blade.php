@props(['nome','img','descrizione'])

@if (Route::currentRouteName('services.index') == 'services.index')

    <div class="col-12 col-md-6 col-xl-4 d-flex justify-content-center">   

@else

    <div class="col-12 d-flex justify-content-center">    

@endif

    
        <div class="card mb-5" style="width: 18rem;">

            <img src="{{$img}}" class="card-img-top img-card2" alt="{{$nome}}">

                <div class="card-body d-flex flex-column align-items-center ">
                    <h3 class="card-title pt-2 text-center">{{$nome}}</h3>

                    @if (Route::currentRouteName('services.show') == 'services.show')
                        <p class="card-text mt-3">{{$descrizione}}</p>
                    @endif

                    <a href="{{Route::currentRouteName('services.index') == 'services.index' ? route('services.show' , ['nome'=>$nome]) : route('services.index')}}" class="btn btn-custom mt-3 py-2 px-3 text-center">{{Route::currentRouteName('services.index') == 'services.index' ? 'Vai al dettaglio' : 'Torna indietro'}}</a>

                </div>

        </div>

    </div>