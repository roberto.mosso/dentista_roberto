<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Il dentista</title>

    <!-- font google -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">

    <link rel="stylesheet" href="/css/app.css">

  </head>

  <body>
   

  
    <x-navbar/>



    
    {{$slot}}

    
    {{-- <div style="margin-top:1000px"></div> --}}


    <x-footer/>



    <script src="/js/app.js"></script>
    <script src="https://kit.fontawesome.com/33417b26e0.js" crossorigin="anonymous"></script>

  </body>
</html>