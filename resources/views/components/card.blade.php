@props(['nome', 'cognome', 'descrizione', 'img'])

<div class="col-12 col-md-6 col-xl-4 d-flex justify-content-center">   
    <div class="card mb-5" style="width: 18rem;">
    <img src="{{$img}}" class="card-img-top img-card" alt="{{$nome}}">
        <div class="card-body">
            <h4 class="card-title pt-2">{{$nome}} {{$cognome}}</h4>
            <p class="card-text pt-3 ">{{$descrizione}}</p>
        </div>
    </div>
</div>