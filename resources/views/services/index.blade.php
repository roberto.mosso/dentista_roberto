<x-layout>
    
    <div class="container mb-4 bg-filter">

        <div class="row d-flex justify-content-center align-items-center text-center my-4">
            <div class="col-12">
                <h1 class="h1">Cosa facciamo</h1>
            </div>
        </div>
  

        <div class="row py-3 fadein">
            
            @foreach ($servizi as $servizio)
                
            <x-servizio

                nome="{{$servizio['nome']}}"
                img="{{$servizio['img']}}"
                descrizione="{{$servizio['descrizione']}}"
            
            />
    
            @endforeach
    
        </div>

    </div>

</x-layout>


