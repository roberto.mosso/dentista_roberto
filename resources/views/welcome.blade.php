<x-layout>


<header class="masthead fadein">
  <div class="container h-50">
    <div class="row h-100 align-items-center">
      <div class="col-12 col-md-7 text-left bg-filter">
        <h1 class="h1">Il vostro dentista di fiducia</h1>
        <p class="h3">Affidatevi a dei veri professionisti</p>
      </div>
    </div>

    <div class="row h-100 align-items-top">
      <div class="col-12 col-md-7 text-left bg-filter">
        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus ab nulla dolorum autem nisi officiis blanditiis voluptatem hic, assumenda aspernatur facere ipsam nemo ratione cumque magnam enim fugiat reprehenderit expedita.</p>
      </div>
      {{-- <h2 class="font-weight-light">Page Content</h2> --}}
      
    </div>
  </div>
</header>


    
</x-layout>