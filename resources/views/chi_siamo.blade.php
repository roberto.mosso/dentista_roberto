<x-layout>

  <div class="container mb-4 bg-filter">
    <div class="row d-flex text-center align-items-center my-4">
      <div class="col-12">
        <h1 class="h1">Chi siamo</h1>
      </div>
    </div>
  
  
  
      <div class="row py-3 fadein">
        
        @foreach ($persone as $persona)
            
          <x-card
          
            nome="{{$persona['nome']}}"
            cognome="{{$persona['cognome']}}"
            {{-- eta="{{$persona['eta']}}" --}}
            img="{{$persona['img']}}"
            descrizione="{{$persona['descrizione']}}"

          />
  
        @endforeach
  
      </div>
  </div>

</x-layout>