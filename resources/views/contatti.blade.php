<x-layout>

  <section class="ftco-section ">
		<div class="container fadein bg-filter">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center my-4">
					<h1 class="heading-section h1">I nostri contatti</h1>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-11">
					<div class="wrapper">
						<div class="row no-gutters justify-content-between">
							<div class="col-lg-6 d-flex align-items-stretch">
								<div class="info-wrap w-100 p-5">
									<h2 class="mb-4">Contattaci</h2>
				        	<div class="dbox w-100 d-flex align-items-start my-3">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-map-marker pt-1"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Indirizzo:</span> Via le dita dal naso 23, Torino</p>
					          </div>
				          </div>
				        	<div class="dbox w-100 d-flex align-items-start my-3">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-phone pt-1"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Telefono:</span> <a href="tel://1234567920" class="a-form">011-1234567 / 333-1234567</a></p>
					          </div>
				          </div>
				        	<div class="dbox w-100 d-flex align-items-start my-3">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-paper-plane pt-1"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Email:</span> <a href="mailto:info@yoursite.com" class="a-form">info@ildentista.com</a></p>
					          </div>
				          </div>
				        	<div class="dbox w-100 d-flex align-items-start my-3">
				        		<div class="icon d-flex align-items-center justify-content-center">
				        			<span class="fa fa-facebook pt-1"></span>
				        		</div>
				        		<div class="text pl-4">
					            <p><span>Facebook:</span> <a href="#" class="a-form">ildentista</a></p>
					          </div>
				          </div>
			          </div>
							</div>
							<div class="col-lg-5">
								<div class="contact-wrap w-100 p-md-4 p-3">
									<h4 class="mb-4 text-center btn-custom2 py-2">Scrivici per informazioni</h4>
									<div id="form-message-warning" class="mb-4"></div> 
				      		<div id="form-message-success" class="mb-4">
				            Il tuo messaggio è stato inviato, grazie!
				      		</div>
									<form action="{{route('contatti.submit')}}" method="POST" id="contactForm" name="contactForm">
										@csrf
										@if ($errors->any())
											<div class="alert alert-danger">
												<ul>
													@foreach ($errors->all() as $error)
														<li>{{ $error }}</li>
													@endforeach
												</ul>
											</div>
										@endif
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<input type="text" class="form-control" name="username" id="name" placeholder="Nome">
												</div>
											</div>
											<div class="col-md-12"> 
												<div class="form-group">
													<input type="email" class="form-control" name="email" id="email" placeholder="Email">
												</div>
											</div>
								
											<div class="col-md-12">
												<div class="form-group">
													<textarea name="message" class="form-control" id="message" cols="30" rows="10" placeholder="Messaggio"></textarea>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<input type="submit" value="Send Message" class="btn btn-custom">
													<div class="submitting"></div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</x-layout>


