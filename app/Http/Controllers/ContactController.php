<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactMail;

class ContactController extends Controller
{
    public function submit(ContactRequest $request) {
        // dd($request->all());

        $username = $request->input('username');
        $email = $request->input('email');
        $message = $request->input('message');

        $contact = compact('username', 'message');

        Mail::to($email)->send(new ContactMail($contact));

        return redirect(route('thank-you'));
    }

    public function thankyou(){
        return view('thank-you');
    }

}
