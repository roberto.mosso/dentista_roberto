<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function homepage() {
    return view('welcome');
    }


    public function chi_siamo() {

        $persone = [
            [
                "nome" => "Lello",
                "cognome" => "Coltello",
                // "eta" => 35,
                "img" => "/img/sweeney.jpg",
                "descrizione" => "Il dentista di punta del nostro studio, col suo carattere affilato saprà farvi perdere la testa"
            ],
            [
                "nome" => "Mario",
                "cognome" => "Rossi",
                // "eta" => 30,
                "img" => "/img/mario_rossi.jpg",
                "descrizione" => "Potrà anche sembrare un pivello, ma le sue capacità e la sua destrezza faranno cadere ogni barriera"
            ],
            [
                "nome" => "Elisabetta",
                "cognome" => "Seconda",
                // "eta" => 85,
                "img" => "/img/regina_elisabetta.jpg",
                "descrizione" => "Tanta esperienza per Elisabetta, decenni di lavoro al servizio dei vostri denti"
            ]
        ];

        return view('chi_siamo', ["persone" => $persone]);
    
    }


    public function contatti()
    {
        return view('contatti');
    }

}
