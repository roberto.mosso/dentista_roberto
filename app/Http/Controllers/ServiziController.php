<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiziController extends Controller
{

    public function index(){

        $servizi = [
            [
                "nome" => "Visite e controlli",
                "img" => "/img/poltrona.jpg",
                "descrizione" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus aperiam ipsa maxime facilis eligendi! Deserunt, nostrum nesciunt! Assumenda, enim iusto! Maxime voluptatem ipsam repellendus facere tempore vel velit ducimus nobis!"
            ],
            [
                "nome" => "Pulizia dei denti",
                "img" => "/img/pulizia.jpg",
                "descrizione" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus aperiam ipsa maxime facilis eligendi! Deserunt, nostrum nesciunt! Assumenda, enim iusto! Maxime voluptatem ipsam repellendus facere tempore vel velit ducimus nobis!"
            ],
            [
                "nome" => "Impianti",
                "img" => "/img/cosa-facciamo1.jpg",
                "descrizione" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus aperiam ipsa maxime facilis eligendi! Deserunt, nostrum nesciunt! Assumenda, enim iusto! Maxime voluptatem ipsam repellendus facere tempore vel velit ducimus nobis!"
            ],
        ];

    
        return view('services.index', ["servizi" => $servizi]);
    }



    public function show($nome)
    {

        $servizi = [
            [
                "nome" => "Visite e controlli",
                "img" => "/img/poltrona.jpg",
                "descrizione" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus aperiam ipsa maxime facilis eligendi! Deserunt, nostrum nesciunt! Assumenda, enim iusto! Maxime voluptatem ipsam repellendus facere tempore vel velit ducimus nobis!"
            ],
            [
                "nome" => "Pulizia dei denti",
                "img" => "/img/pulizia.jpg",
                "descrizione" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus aperiam ipsa maxime facilis eligendi! Deserunt, nostrum nesciunt! Assumenda, enim iusto! Maxime voluptatem ipsam repellendus facere tempore vel velit ducimus nobis!"
            ],
            [
                "nome" => "Impianti",
                "img" => "/img/cosa-facciamo1.jpg",
                "descrizione" => "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatibus aperiam ipsa maxime facilis eligendi! Deserunt, nostrum nesciunt! Assumenda, enim iusto! Maxime voluptatem ipsam repellendus facere tempore vel velit ducimus nobis!"
            ],
        ];

        foreach ($servizi as $servizio) {
            if($servizio['nome'] == $nome){
                return view('services.show', ["servizio" => $servizio]);
            }
        }

    }

    
}
