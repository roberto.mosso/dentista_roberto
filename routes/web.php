<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ServiziController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, "homepage"])->name("homepage");


Route::get('/chi_siamo', [PublicController::class, "chi_siamo"])->name("chi_siamo");


Route::get('/servizi', [ServiziController::class, "index"])->name("services.index");


Route::get('/servizi/{nome}', [ServiziController::class, "show"])->name("services.show");


Route::get('/contatti', [PublicController::class, "contatti"])->name("contatti");


Route::post('/contatti/submit', [ContactController::class, "submit"])->name("contatti.submit");


Route::get('/thank-you', [ContactController::class, "thankyou"])->name("thank-you");
